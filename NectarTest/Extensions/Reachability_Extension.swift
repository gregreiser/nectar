//
//  Reachability_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation
import Reachability

extension Reachability {
    private static var instance: Reachability?
    
    static func isReachable() -> Bool {
        if instance == nil {
            instance = Reachability.forInternetConnection()
        }
        
        return instance?.isReachable() ?? false
    }
}
