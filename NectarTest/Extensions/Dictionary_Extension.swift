//
//  Dictionary_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

extension Dictionary {
    func toData() -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        } catch _ {}
        return nil
    }
}
