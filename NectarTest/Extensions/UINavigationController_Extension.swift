//
//  UInavigationController_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

extension UINavigationController {
    
    func makeTransparent(showSeparator: Bool = true) {
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        navigationBar.barStyle = .black
        if showSeparator {
            addSeperatorIfNecessary()
        }
    }

    private func addSeperatorIfNecessary() {
        
        let idForSeparatorView = 9001
        
        for subView in navigationBar.subviews {
            if subView.tag == idForSeparatorView {
                return
            }
        }
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor.white
        separatorView.tag = idForSeparatorView
        navigationBar.addSubview(separatorView)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.widthAnchor.constraint(equalTo: navigationBar.widthAnchor).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
}
