//
//  UIImage_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit
import Kingfisher

extension UIImageView {
    public func setRemoteImage(withStringUrl url: String?, placeholderName: String? = nil, hasTransition: Bool = true, completionHandler: (()->())? = nil) {
        guard let urlString = url, !urlString.isEmpty else {
            self.image = UIImage(named: placeholderName!)
            return
        }
        
        let options: KingfisherOptionsInfo = [
            .scaleFactor(UIScreen.main.scale),
            .cacheOriginalImage,
            .keepCurrentImageWhileLoading,
            .transition(hasTransition ? .fade(0.3) : .none),
        ]
        
        self.kf.setImage(with: URL(string: urlString),
                         placeholder: placeholderName != nil ? UIImage(named: placeholderName!) : nil,
                         options: options) { (result) in
            completionHandler?()
        }
    }
}

