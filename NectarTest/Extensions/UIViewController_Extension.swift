//
//  UIViewController_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

extension UIViewController {
    
    func presentInNavigationController(viewController: UIViewController, modalStyle: UIModalPresentationStyle = UIModalPresentationStyle.fullScreen, showNavBar: Bool = true, completion: (() -> Void)?) {
        let navigationController = viewController.initInNavigationController(modalStyle: modalStyle, showNavBar: showNavBar)
        if let presentationDelegate = viewController as? UIAdaptivePresentationControllerDelegate {
            navigationController.presentationController?.delegate = presentationDelegate
        }
        self.present(navigationController, animated: true, completion: completion)
    }
    
    func initInNavigationController(modalStyle: UIModalPresentationStyle = UIModalPresentationStyle.fullScreen, showNavBar: Bool = true) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: self)
        navigationController.isNavigationBarHidden = !showNavBar
        navigationController.modalPresentationStyle = modalStyle
        navigationController.navigationBar.barTintColor = Colors.navBar
        navigationController.navigationBar.titleTextAttributes = [.foregroundColor: Colors.accent]
        navigationController.navigationBar.tintColor = Colors.accent
        return navigationController
    }
}
