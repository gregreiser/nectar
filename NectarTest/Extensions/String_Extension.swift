//
//  String_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

extension String {
    var localized: String {
      return NSLocalizedString(self, comment: "\(self)_comment")
    }
}
