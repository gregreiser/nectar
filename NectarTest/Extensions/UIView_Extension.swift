//
//  UIView_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

extension UIView {
    
    func setGradientBackground(topColor: UIColor, bottomColor: UIColor, center: NSNumber = 0) {
        let gradientName = "gradient"
        let gradientLayer = CAGradientLayer()
        gradientLayer.name = gradientName
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.locations = [0.0, center, 1.0]
        gradientLayer.frame = self.bounds
        
        for layer in layer.sublayers ?? [] {
            if layer.name == gradientName {
                layer.removeFromSuperlayer()
            }
        }
        self.layer.insertSublayer(gradientLayer, at:0)
    }
}
