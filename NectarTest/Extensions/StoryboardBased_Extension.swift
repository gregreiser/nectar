//
//  StoryboardBased_Extension.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

// MARK: Protocol Definition

/// Make your UIViewController subclasses conform to this protocol when:
///  * they *are* Storyboard-based, and
///  * this ViewController is the inside of your Storyboard
///
/// to be able to instantiate multiple viewControllers from the same Storyboard in a type-safe manner
public protocol StoryboardBased where Self: UIViewController {
    static var sceneStoryboard: UIStoryboard { get }
    static var storyboardName: String { get }
}

public extension StoryboardBased {
    
    static var sceneStoryboard: UIStoryboard {
        return UIStoryboard(name: Self.storyboardName, bundle: Bundle(for: self))
    }
    
    static var storyboardName: String {
        return String(describing: self)
    }

    static func instantiate(modalPresentationStyle: UIModalPresentationStyle = .fullScreen) -> Self {
        guard let vc = sceneStoryboard.instantiateViewController(withIdentifier: String(describing: self)) as? Self else {
            fatalError("The controller of '\(sceneStoryboard)' is not of class '\(self)'")
        }
        vc.modalPresentationStyle = modalPresentationStyle
        return vc
    }
}
