//
//  UserDefaultsManager.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

class UserDefaultsManager {
    
    static func save(hives: [HiveModel]) {
        guard let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: hives, requiringSecureCoding: false) else { return }
        UserDefaults.standard.set(encodedData, forKey: UserDefaultsKeys.hives)
    }
    
    static func fetchHives() -> [HiveModel] {
        guard let data = UserDefaults.standard.data(forKey: UserDefaultsKeys.hives) else { return [] }
        let hives = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [HiveModel]
        return hives ?? []
    }
    
    static func save(pendingRequests: [URLRequest]) {
        var requestDicts = [[String:String]]()
        for request in pendingRequests {
            var dict = [String:String]()
            dict["urlString"] = request.url?.absoluteString
            dict["httpMethod"] = request.httpMethod
            requestDicts.append(dict)
        }
        UserDefaults.standard.set(requestDicts, forKey: UserDefaultsKeys.pendingRequest)
    }
    
    static func fetchPendingRequests() -> [URLRequest] {
        guard let requestDicts = UserDefaults.standard.array(forKey: UserDefaultsKeys.pendingRequest) as? [[String:String]] else { return [] }
        var requests = [URLRequest]()
        for dict in requestDicts {
            guard let url = URL(string: dict["urlString"] ?? "") else { continue }
            var request = URLRequest(url: url)
            request.httpMethod = dict["httpMethod"]
            requests.append(request)
        }
        return requests
    }
}
