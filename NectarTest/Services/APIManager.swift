//
//  APIManager.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation
import Reachability

class APIManager {
    
    enum HttpMethod: String {
        case DELETE
        case GET
    }
    
    enum FailureReason: String {
        case noNetwork
        case other
    }
    
    static var instance: APIManager!
    private static let baseUrl = "https://60a3cfee7c6e8b0017e27f64.mockapi.io/api/v1"
    private var pendingRequests = [URLRequest]()
    
    required init() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        pendingRequests = UserDefaultsManager.fetchPendingRequests()
        handlePendingRequest()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.reachabilityChanged, object: nil)
    }
    
    @objc func reachabilityChanged(notif: NSNotification) {
        handlePendingRequest()
    }
    
    private func handlePendingRequest() {
        guard Reachability.isReachable() else { return }
        if let request = pendingRequests.first {
            APIManager.resumeUrlSession(with: request) { data in
                self.pendingRequests.removeAll { urlRequest in
                    urlRequest == request
                }
                self.handlePendingRequest()
            } failure: { reason in
                print("Request error for reason: \(reason.rawValue)") //For this test, i will stop here but to be more efficient, we could get the error code and handle different behaviour depends on it.
            }
        }
    }
}

//--------------- Generic Methods ------------------------
extension APIManager {
    
    private static func savePendingRequest(_ request: URLRequest) {
        instance.pendingRequests.append(request)
        UserDefaultsManager.save(pendingRequests: instance.pendingRequests)
    }
    
    private static func loadPendingRequest(_ request: URLRequest) {
        instance.pendingRequests = UserDefaultsManager.fetchPendingRequests()
    }
    
    static func makeRequest(with url: URL, httpMethod: HttpMethod, success: @escaping (_ data: Data)->(), failure: ((_ reason: FailureReason)->())? = nil){
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        guard Reachability.isReachable() else {
            savePendingRequest(request)
            failure?(.noNetwork)
            return
        }
        
        resumeUrlSession(with: request, success: success, failure: failure)
    }
    
    static func resumeUrlSession(with request: URLRequest, success: @escaping (_ data: Data)->(), failure: ((_ reason: FailureReason)->())? = nil){
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil, let data = data else { // We could check the status code to save request for later is the server dosn't respond
                failure?(.other)
                return
            }
            success(data)
        }.resume()
    }
}

//--------------- Hive Methods ------------------------
extension APIManager {
    
    static func fetchHiveList(success: @escaping (_ jsonResult: [[String: Any]])->(), failure: ((_ reason: FailureReason)->())? = nil) {
        guard let url = URL(string: "\(baseUrl)/hives") else { return }
        
        makeRequest(with: url, httpMethod: .GET) { data in
            let jsons = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [[String:Any]]
            success(jsons ?? [])
        } failure: { reason in
            failure?(reason)
        }
    }
    
    static func deleteHive(with id: String, success: @escaping ()->(), failure: ((_ reason: FailureReason)->())? = nil) {
        guard let url = URL(string: "\(baseUrl)/hives/\(id)") else { return }
    
        makeRequest(with: url, httpMethod: .DELETE) { data in
            success()
        } failure: { reason in
            failure?(reason)
        }
    }
}

