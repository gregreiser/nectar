//
//  AlertUtils.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-25.
//

import Foundation
import PMAlertController

class AlertUtils {
    
    class func showConfirmationDialog(title: String, description: String, actionTitle: String, callback: @escaping ()->()) {
        let alertVC = PMAlertController(title: title, description: description, image: nil, style: .alert)
        alertVC.addAction(PMAlertAction(title: "Annuler".localized, style: .cancel, action: nil))
        alertVC.addAction(PMAlertAction(title: actionTitle.localized, style: .default, action: { () in
            callback()
        }))
        UIApplication.topViewController()?.present(alertVC, animated: true, completion: nil)
    }
}
