//
//  HivesManager.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

@objc protocol HivesManagerObserver {
    func hivesUpdated(hives: [HiveModel])
}

class HivesManager {
    
    static var instance: HivesManager!
    private var observers = NSHashTable<HivesManagerObserver>.weakObjects()
    var hives = [HiveModel]() {
        didSet {
            self.saveHivesInUserDefaults()
        }
    }
    
    
    init() {
        APIManager.fetchHiveList { jsonResult in
            self.createHives(from: jsonResult)
        } failure: { reason in
            self.fetchHivesFromUserDefaults()
        }
    }
    
    private func saveHivesInUserDefaults() {
        UserDefaultsManager.save(hives: hives)
    }
    
    private func fetchHivesFromUserDefaults() {
        hives = UserDefaultsManager.fetchHives()
    }
    
    static func add(observer: HivesManagerObserver) {
        if !instance.observers.contains(observer) {
            instance.observers.add(observer)
        }
    }
    
    static func remove(observer: HivesManagerObserver) {
        instance.observers.remove(observer)
    }
    
    static func removeHive(with id: String, success: @escaping ()->(), failure: (()->())?) {
        APIManager.deleteHive(with: id) {
            DispatchQueue.main.async {
                removeLocalHive(with: id)
                success()
            }
        } failure: { reason in
            if reason == .noNetwork {
                DispatchQueue.main.async {
                    removeLocalHive(with: id)
                    success()
                }
                return
            }
            print("Error during deleting")
        }
    }
    
    private static func removeLocalHive(with id: String) {
        instance.hives.removeAll { hiveModel in
            hiveModel.id == id
        }
        instance.notifyHivesUpdated()
    }
    
    private func createHives(from jsons: [[String: Any]]) {
        hives = []
        for hiveJson in jsons {
            let decoder = JSONDecoder()
            if let hiveData = hiveJson.toData(), let hiveModel = try? decoder.decode(HiveModel.self, from: hiveData) {
                hives.append(hiveModel)
            }
        }
        DispatchQueue.main.async {
            self.notifyHivesUpdated()
        }
    }
    
    private func notifyHivesUpdated() {
        for observer in observers.allObjects {
            observer.hivesUpdated(hives: hives)
        }
    }
}
