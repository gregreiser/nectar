//
//  LargeImageHiveTableViewCell.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

class LargeImageHiveTableViewCell: BasicHiveTableViewCell {
    
    @IBOutlet private weak var textView: UIView!
    @IBOutlet private weak var mainView: UIView!
    
    override func configure(with hiveModel: HiveModel) {
        super.configure(with: hiveModel)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 6
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor.darkGray.withAlphaComponent(0.2).cgColor
        self.backgroundColor = .clear
    }
}
