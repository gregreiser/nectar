//
//  SmallHiveTableViewCell.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

class SmallHiveTableViewCell: BasicHiveTableViewCell {
        
    @IBOutlet private weak var textview: UIView!
    
    override func configure(with hiveModel: HiveModel) {
        super.configure(with: hiveModel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mainImageView.layer.cornerRadius = mainImageView.frame.size.height / 2
        textview.layer.cornerRadius = 12
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
}
