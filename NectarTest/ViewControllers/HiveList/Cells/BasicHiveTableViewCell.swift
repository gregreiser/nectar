//
//  BasicHiveTableViewCell.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

class BasicHiveTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    
    func configure(with hiveModel: HiveModel) {
        self.nameLabel.text = hiveModel.name
        self.dateLabel.text = "Crée le : \(DateUtils.readabledDate(from: hiveModel.createdAt))"
        self.mainImageView.setRemoteImage(withStringUrl: hiveModel.image, placeholderName: "nectar_placeholder")
    }
}
