//
//  HiveListPresenter.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

@objc protocol HiveListDelegate {
    func hivesDidChange()
}

class HiveListPresenter: NSObject {
    
    private weak var delegate: HiveListDelegate?
    
    required init(delegate: HiveListDelegate) {
        super.init()
        self.delegate = delegate
        HivesManager.add(observer: self)
    }
    
    deinit {
        HivesManager.remove(observer: self)
    }
    
    func hivesNumber() -> Int {
        return HivesManager.instance.hives.count
    }
    
    func hiveModel(at index: Int) -> HiveModel? {
        return index < hivesNumber() ? HivesManager.instance.hives[index] : nil
    }
}

extension HiveListPresenter: HivesManagerObserver {
    func hivesUpdated(hives: [HiveModel]) {
        self.delegate?.hivesDidChange()
    }
}
