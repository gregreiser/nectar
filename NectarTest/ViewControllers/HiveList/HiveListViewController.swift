//
//  HiveListViewController.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

class HiveListViewController: UIViewController, StoryboardBased {
    
    private let largeImageIdentifier = "LargeImageHiveTableViewCell"
    private let smallImageIdentifier = "SmallHiveTableViewCell"
    private var presenter: HiveListPresenter?
    @IBOutlet private weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Nectar"
        self.presenter = HiveListPresenter(delegate: self)
        registerCells()
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: largeImageIdentifier, bundle: nil), forCellReuseIdentifier: largeImageIdentifier)
        tableView.register(UINib(nibName: smallImageIdentifier, bundle: nil), forCellReuseIdentifier: smallImageIdentifier)
    }
}

extension HiveListViewController: HiveListDelegate {
    func hivesDidChange() {
        tableView.reloadData()
    }
}

extension HiveListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.hivesNumber() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let hiveModel = presenter?.hiveModel(at: indexPath.row) else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: smallImageIdentifier) as! SmallHiveTableViewCell
        cell.selectionStyle = .none
        cell.configure(with: hiveModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let hiveModel = presenter?.hiveModel(at: indexPath.row) else { return }
        self.navigationController?.pushViewController(HiveDetailViewController.instansiate(hiveModel: hiveModel), animated: true)
    }
}

