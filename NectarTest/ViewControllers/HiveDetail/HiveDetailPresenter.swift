//
//  HiveDetailPresenter.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

@objc protocol HiveDetailDelegate {
    func hiveDeleted()
}

class HiveDetailPresenter: NSObject {
    
    private weak var delegate: HiveDetailDelegate?
    var hive: HiveModel!
    
    required init(delegate: HiveDetailDelegate, hiveModel: HiveModel) {
        super.init()
        self.delegate = delegate
        self.hive = hiveModel
    }
    
    deinit {
    }
    
    func deleteClicked() {
        HivesManager.removeHive(with: hive.id, success: {
            self.delegate?.hiveDeleted()
        }, failure: {
            print("Error during deleting")
        })
    }
}
