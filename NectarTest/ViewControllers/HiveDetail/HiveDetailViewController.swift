//
//  HiveDetailViewController.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit
import PMAlertController

class HiveDetailViewController: UIViewController, StoryboardBased {
    
    private var presenter: HiveDetailPresenter?
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var creationDateLabel: UILabel!
    @IBOutlet private weak var deleteButton: UIButton!
    
    static func instansiate(hiveModel: HiveModel) -> HiveDetailViewController {
        let vc = HiveDetailViewController.instantiate()
        vc.presenter = HiveDetailPresenter(delegate: vc, hiveModel: hiveModel)
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = presenter?.hive.name ?? ""
        self.imageView.setRemoteImage(withStringUrl: presenter?.hive.image, placeholderName: "nectar_placeholder")
        self.creationDateLabel.text = "Date de création : \(DateUtils.readabledDate(from: presenter?.hive.createdAt ?? ""))"
        self.deleteButton.backgroundColor = Colors.irreversible
        self.deleteButton.setTitle("Supprimer".localized, for: .normal)
        self.deleteButton.setTitleColor(UIColor.white, for: .normal)
        self.deleteButton.layer.cornerRadius = self.deleteButton.frame.height / 2
    }
    
    @IBAction func deleteClicked(_ sender: Any) {
        AlertUtils.showConfirmationDialog(title: "Etes-vous sûr ?".localized, description: "Cette action est irréversible.".localized, actionTitle: "Supprimer") {
            self.presenter?.deleteClicked()
        }
    }
}

extension HiveDetailViewController: HiveDetailDelegate {
    func hiveDeleted() {
        self.navigationController?.popViewController(animated: true)
    }
}
