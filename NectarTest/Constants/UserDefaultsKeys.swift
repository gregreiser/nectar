//
//  UserDefaultsKeys.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

class UserDefaultsKeys {
    static let pendingRequest = "pendingRequests"
    static let hives = "hives"
}
