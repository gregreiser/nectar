//
//  Colors.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import UIKit

class Colors {
    static let irreversible = UIColor(red: 240/255, green: 128/255, blue: 128/255, alpha: 1)
    static let navBar = UIColor.white
    static let accent = UIColor(red: 209/255, green: 164/255, blue: 88/255, alpha: 1)
}
