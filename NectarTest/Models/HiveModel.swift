//
//  HiveModel.swift
//  NectarTest
//
//  Created by Gregory Reiser on 2021-05-24.
//

import Foundation

class HiveModel: NSObject, NSCoding, Codable {
    var id: String
    var createdAt: String
    var name: String
    var image: String
    
    enum Keys: String {
        case id = "id"
        case createdAt = "createdAt"
        case name = "name"
        case image = "image"
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: Keys.id.rawValue)
        coder.encode(createdAt, forKey: Keys.createdAt.rawValue)
        coder.encode(name, forKey: Keys.name.rawValue)
        coder.encode(image, forKey: Keys.image.rawValue)
    }
    
    required init?(coder: NSCoder) {
        self.id = coder.decodeObject(forKey: Keys.id.rawValue) as! String
        self.createdAt = coder.decodeObject(forKey: Keys.createdAt.rawValue) as! String
        self.name = coder.decodeObject(forKey: Keys.name.rawValue) as! String
        self.image = coder.decodeObject(forKey: Keys.image.rawValue) as! String
    }
}
